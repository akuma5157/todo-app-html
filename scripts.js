

function submitTaskForm(event) {
  event.preventDefault();

  var newTaskBox = document.getElementById("newTaskBox");
  if (newTaskBox.value === '') {
    alert("You must write something!");
  } else {
    addTask(newTaskBox.value, false);
    newTaskBox.value = '';
  }
}

function addTask(task, done) {
  var div = document.createElement("div");
  div.innerText = task;
  div.classList.add('taskText');

  var input = document.createElement("input");
  input.type = 'checkbox';
  input.checked = done;

  i = document.createElement("i");
  i.classList.add("ri-delete-bin-7-line")
  i.style.fontSize = '2vw'
  i.style.color = '#bdbdbd'
  button = document.createElement("button");
  button.classList.add("deleteTaskButton");
  button.appendChild(i);
  button.style.display = 'none'
  button.onclick = function() {deleteTask(this);}
  
  var label = document.createElement("label");
  label.classList.add("taskLabel");
  label.appendChild(input);
  label.appendChild(div);
  label.appendChild(button);

  var li = document.createElement("li");
  li.classList.add("taskItem")
  li.appendChild(label);

  document.getElementById("taskList").appendChild(li);
}

function showCloseButtonOnTask(taskItem, visibility) {
  taskItem.children[0].children[2].style.display = visibility ? '' : 'none';
}

function showDeleteAllButton(visibility) {
  document.getElementById('deleteAllButton').style.display = visibility ? '' : 'none';
}

function showAddForm(visibility) {
  document.getElementById('newTaskForm').style.display = visibility ? '' : 'none';
}

function setActiveTab(tabName) {
  //reset items
  var tabs = document.getElementsByClassName('activeTab');
  [].forEach.call(tabs, function (tab) {tab.classList.remove('activeTab')});
  document.getElementById(tabName).classList.add('activeTab');

  
  var taskItems = document.getElementsByClassName('taskItem');
  
  if (tabName === 'completed') {
    showDeleteAllButton(true);
    showAddForm(false);

    [].forEach.call(taskItems, function (taskItem) {
      showCloseButtonOnTask(taskItem, true);
      // set visibility
      if(taskItem.children[0].children[0].checked === false) {
        taskItem.style.display = "none"
      }
      else {
        taskItem.style.display = ""
      }
    });
  }
  else {
    showDeleteAllButton(false);
    showAddForm(true);
    [].forEach.call(taskItems, function (taskItem) {
      showCloseButtonOnTask(taskItem, false);
      if(taskItem.children[0].children[0].checked === true && tabName === 'active'){
        taskItem.style.display = "none";
      }
      else {
        taskItem.style.display = ""
      }
    });
  }
}

function deleteTask(task) {
  taskText = task.parentElement.children[1].innerHTML
  if (confirm(`are you sure you want to delete this task?!\n${taskText}`) == true) {
    task.parentElement.parentElement.remove();
  }
}
function deleteAllCompletedTasks() {
  console.log(document.getElementsByTagName('li'));
  if (confirm("are you sure you want to delete all completed tasks?!") == true) {
    [].forEach.call(document.getElementsByTagName('li'), function (taskItem) {
      if(taskItem.children[0].children[0].checked === true) {
        console.log(taskItem.children[0].children[1].innerHTML);
        taskItem.remove();
      }
    });
  }
}

window.addEventListener('DOMContentLoaded', () => {
  var allTasks = [
    ['Do Coding Challenges', true],
    ['Do More Coding Challenges', false],
    ['Do Even More Coding Challenges', false]
  ];
  [].forEach.call(allTasks, function (taskItem) {
    addTask(taskItem[0], taskItem[1]);
  });
  setActiveTab('all');
});
